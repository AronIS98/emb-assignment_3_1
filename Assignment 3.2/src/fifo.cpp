
#include <fifo.h>

Fifo::Fifo()
{
// int buffer[FIFO_SIZE];
end = 0;
int* pointer = buffer;
point_start = 0;
point_end = -1;

}

int Fifo::get()
{
    // buffer[]
    int return_val = buffer[0];

    for(int i = 0; i < FIFO_SIZE-1;i++)
        {
        buffer[i] = buffer[i+1];
        }
    buffer[FIFO_SIZE-1] = 0;

    if(end >0){end--;}
    
    return return_val;
}

void Fifo::put(int item)
{

if(end>=(FIFO_SIZE-1)){int disgard = Fifo::get();}
buffer[end] = item;
end++;
}

bool Fifo::is_empty()
{
    return end == 0;
}

bool Fifo::is_full()
{
    return end == (FIFO_SIZE-1);
}

void Fifo::reset()
{
    end = 0;
}



int Fifo::circ_get()
{
    int out = *(pointer+point_start);
    if(point_end != point_start){point_start = (point_start+1)%FIFO_SIZE;}
    
    return out;
}

void Fifo::circ_put(int item)
{   
    
    if((point_start == ((point_end+1))%FIFO_SIZE)&&point_end != -1){point_start = (point_start + 1)%FIFO_SIZE;}
    point_end = (point_end + 1)%FIFO_SIZE;   

    *(pointer+point_end) = item;
}

bool Fifo::circ_is_empty()
{
    return point_start==point_end;
}

bool Fifo::circ_is_full()
{
    return point_start == (point_end + 1)%FIFO_SIZE;
}

void Fifo::circ_reset()
{
    point_end = 0;
    point_start = 0;
    *pointer = 0;
}

